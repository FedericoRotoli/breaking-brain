//
//  ShakeController3.swift
//  provaMain
//
//  Created by Federico Rotoli on 20/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//


import UIKit
import CoreData

class ShakeController3: UIViewController {
    
    @IBOutlet weak var spinePart: UILabel!
    @IBOutlet weak var phraseLabel: UILabel!
    @IBOutlet weak var shakeLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    var untilFinally: [String] = ["Died","Married","Bought an house","Became rich","Became fit","Became muscular","Defeat fears","Made friends","Ate the forbidden food","Became a comedian","Created a new country","The ritual was complete","Changed his clothes","Killed him","Raided the bank","Made a dream come true","Went to the moon","Adopted a dog","Bought a tv","Wrote a book","Became skinny","Lost weight","Unlocked the hidden powers","Sat down on a chair","Discovered himself","Found the hidden treasure","Summoned the desired deity","Found the lost pirate ship","Had a child","Lost all his belongings","The virus was freed","The window was broken","The bycicle was repaired","Defeated the evil guys","Conquered the desired love","Learned to walk again","Crushed someone in a tv","Escaped from an alternate reality","The aliens defeated the humans"]
    
    var everSinceThen: [String] = ["Lived happily after","Ate all food in the fridge everyday","Never feared pears anymore","Lived in a fridge","Removed everyone’s clothes everyday","Started to hate each other","Killed all farmers in the world","Made Silvio Berlusconi cry everyday","Defeated all of the bad people","Built an empire","Destroyed the universe","Became the hero who saved everyone","Bought anything he wanted","Every child in the world became happy","All the clowns in the world went to the moon","All the Italian television went down","The recipe of the secret sauce was discovered","Never ate sugar again","Never doubted anybody ever again","The zombies kept killing people","Never played games anymore","Got sent to prison","Never resurrected the dead ever again","Every window in the building exploded","Aliens conquered earth"]
    
    var phrase1 = ""
    var phrase3 = ""
    
    var firstTime: Bool = true
    
    var times: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        spinePart.layer.cornerRadius = 5
        UIView.animate(withDuration: 1.0, delay: 0.1, options: [.repeat,.curveEaseInOut,.autoreverse], animations: {
            self.shakeLabel.alpha=0.2
        }, completion: nil)
        
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if firstTime {
            
            firstTime = false
            
            if motion == .motionShake {
                switch times {
                case 0:
                    let index = Int.random(in: 0 ..< untilFinally.count)
                    phrase1 = untilFinally[index]
                    phraseLabel.text = phrase1
                    phraseLabel.sizeToFit()
                        
                    phraseLabel.center = view.center
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                    
                default:
                    let index = Int.random(in: 0 ..< everSinceThen.count)
                    phrase3 = everSinceThen[index]
                    phraseLabel.text = phrase3
                    phraseLabel.sizeToFit()
                        
                    phraseLabel.center = view.center
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                    
                }
            }
            
        }

    }
    
    @IBAction func nextStep(_ sender: Any) {
        
        firstTime = true
        
        switch times {
        case 0:
            
            times += 1
            nextButton.isEnabled = false
            shakeLabel.isHidden = false
            
            spinePart.text = "And, ever since then"
            
            spinePart.sizeToFit()
            spinePart.center.x = view.center.x
            
            phraseLabel.text = "Get ready to storm!"
            phraseLabel.sizeToFit()
            phraseLabel.center = view.center
            
        default:
            performSegue(withIdentifier: "reviewView3", sender: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "reviewView3" {
            if let nextViewController = segue.destination as? ReviewController3 {
                nextViewController.first = phrase1
                nextViewController.third = phrase3
            }
        }
    }
}
