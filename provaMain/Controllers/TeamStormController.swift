//
//  TeamStormController.swift
//  provaMain
//
//  Created by Federico Rotoli on 13/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

class TeamStormController: UIViewController, UITextViewDelegate {
    var db:[NSManagedObject] = []
    
    @IBOutlet weak var textView: UITextView!
    var bool100=true
    var bool200=true
    var bool300=true
    var bool400=true

    @IBOutlet weak var btnTeamStorm: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    var stringComplete = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnTeamStorm.layer.cornerRadius = 15
        btnTeamStorm.imageView?.layer.cornerRadius = 15
        
        let infoButton = UIButton(type: .infoLight)
        let barButton = UIBarButtonItem(customView: infoButton)
        infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = barButton
        
        textView.delegate = self
        
      
    }
    
   func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    

       let currentText = textView.text ?? ""

       guard let stringRange = Range(range, in: currentText) else { return false }

       let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
    if((updatedText.count == 100 && bool100)) {
        bool100=false
        textView.text += " "
    let alert = UIAlertController(title: "Round is Over!", message: "You've exhausted the words for this round. Please pass the phone to the next player and start again!", preferredStyle: .alert)

    alert.addAction(UIAlertAction(title: "Start next round!", style: .default, handler: nil))

    self.present(alert, animated: true)
    }
    
    if((updatedText.count == 200 && bool200)) {
         bool200=false
         textView.text += " "
     let alert = UIAlertController(title: "Round is Over!", message: "You've exhausted the words for this round. Please pass the phone to the next player and start again!", preferredStyle: .alert)

     alert.addAction(UIAlertAction(title: "Start next round!", style: .default, handler: nil))

     self.present(alert, animated: true)
     }
    
    if((updatedText.count == 300 && bool300)) {
         bool300=false
         textView.text += " "
     let alert = UIAlertController(title: "Round is Over!", message: "You've exhausted the words for this round. Please pass the phone to the next player and start again!", preferredStyle: .alert)

     alert.addAction(UIAlertAction(title: "Start next round!", style: .default, handler: nil))

     self.present(alert, animated: true)
     }
    
    if((updatedText.count == 400 && bool400)) {
         bool400=false
         textView.text += " "
     let alert = UIAlertController(title: "Round is Over!", message: "You've exhausted the words for this round. Please pass the phone to the next player and start again!", preferredStyle: .alert)

     alert.addAction(UIAlertAction(title: "Start next round!", style: .default, handler: nil))

     self.present(alert, animated: true)
     }
        
    if (updatedText.count == 500) {
        let alert = UIAlertController(title: "Game Over!", message: "You've exhausted the words for this game. If you want you can save what you wrote but if you failed to conclude you lost!", preferredStyle: .alert)

           alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))

           self.present(alert, animated: true)
           btnSave.isHidden = false;
           textView.isEditable = false
    }
       return updatedText.count <= 500
    
   }
    
    @objc func infoButtonTapped() {
        performSegue(withIdentifier: "infoView2", sender: nil)
    }
    
    @IBAction func btnSaveTapped() {
        stringComplete = textView.text
        
        save(name: stringComplete)
        let alert = UIAlertController(title: "Good Job", message: "Your story has been successfully saved in the portfolio!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes, i got it!", style: .default, handler: {(action: UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true)
      }
    
    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "infoView2" {
            if let nextViewController = segue.destination as? InfoController {
                nextViewController.testo = "This is a game where you have to create a story.\n\nEach player has a limit cap of letters that they can use.\nTo create a story. As soon the limit Is reached they need to pass.\nThe phone to another player which can continue the story or Modify the story that was written by the previous player.\nThe game ends when a full story within the limit of the set amount of letters is created.\n\nA little reminder: even when you delete something you will use the available letters so be careful!\n\nHave fun!"
                nextViewController.color = "yellow"
                nextViewController.image = UIImage(named: "Team Storm.png")
            }
        }
    }
    
    
    func save(name: String) {
        let date = NSDate()
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var dateString = dateFormatter.string(from: date as Date)
    
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
        }
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "FullStory", in: managedContext)!
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        person.setValue(name, forKeyPath: "phrase")
        person.setValue(dateString, forKeyPath: "date")
        // 4
        do {
        try managedContext.save()
            db.append(person)
          } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
        }
        

    }

}
