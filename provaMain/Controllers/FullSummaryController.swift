//
//  FullSummaryController.swift
//  provaMain
//
//  Created by Federico Rotoli on 22/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

class FullSummaryController: UITableViewController {
    
    var fullStory: [NSManagedObject] = []
    
    var onceUponATime: [String] = ["An accountant","A giant octopus","A pizzaman","A pizza with long legs","A giant walking gun","A tank driver","A superhero","A pumpkin man","A cyclist","A coder","A designer","Beppe Braida","A personal trainer","A boat that can talk","A runner","A comedian","The tallest man in the world","A goblin","A creature made of ink","A jellyfish made of chocolate","A baguette named John","A flower named flower","A clown","A jedi","A table named jerry","A soldier with a water gun","Bruno Vespa","Keanu Reeves","A giant hound dog with 4 hands","A bald guy with 6 hands","A sentient sword","An apple shaped dragon","A dog","A policeman","A knight","A man","An alien","A cat","A dragon","A boy named Franco","Maurizio Costanzo","A purple sponge","A boat with eyes","Thomas the train","A goat","A Nerdy guy","An astronaut","A 5 legged apple","A samurai ninja","A vegetarian shark","A vegan lion","A farmer","An apple engineer","A banana with muscles","A man without pants","Son Goku","A giant","A dog that can talk","A warrior","A Neapolitan","A pizza delivery guy","Mentor Francesco","A giraffe with IQ of 600","Claudio Bisio"]
    
    var everyday: [String] = ["Woke up","Counted the stars","Fight","Went to jogging","Made cakes","Cooked","Did codes","Did design","Liked to read","Liked to eat","Killed someone","Stole from someone","Used a sword","Made cookies","Jumped the rope","Watched anime","Watched movies","Feared fruits","Threw rocks trees","Trained","Liked to drive","Works at an animal shop","Hated to be himself","People passed by without saying hi","Ate unhealthy food","Was struggling with excess fat","Invented something","Beat up evil guys","Built bridges","Forged swords","Jumped a rope","Learned something new","Studied Japanese","Combed other people hairs"]
    
    var butOneDay: [String] = ["Went away","Fled","Got lost","Fell","Ate an entire bowl of pasta","The pc was broken","Found a treasure","Wanted to become stronger","Broke a window","Wanted to become taller","Wanted to become better","Surpassed the speed limit","Died","A friend arrived","Started to drive a car","Stole something","Ate a magician","Adopted a yeti","Started to eat healthy","Killed an alien","Bought an house","Became a magician","Stole an iPhone","Stole a pair of shoes","Ate a table","Forged excalibur","Stole a television","Bought a dinosaur","Traveled around the world","Got a bad bad fever","Opened the forbidden door of a temple","Discovered the fountain of youth","Learned the Kamehameha","Discovered hidden powers","Kicked a guy to the moon","Survived an earthquake"]
    
    var becauseOfThat: [String] = ["Went to Japan","Started to go to the gym","Fight other people","Joined mafia","Joined apple academy","Left school","Met a girl","Met a boy","Started to train","Started to play games","Started to run","Started to do scuba dive","Did the Naruto run","Became an anime fan","Became a pineapple","Ate a pizza","Ate a person","Ate a plasma tv","Run into a wall","Went to the moon","Explore the galaxy","Got scared","Got hurt","Surpassed the speed limit","Became a ghost","Was hungry","Was sad","Started a fight","Became a movie star","Became a pop star","Opened a restaurant","Made a game","Followed a guy","Sent some mails","Captured a tiger","Resurrected a dead body","A giant monster killed someone","Destroyed a wall","Hide something","Hijacked a pc","Stole an horse","Fight a tiger","Became a superhero","Jumped under a car","Died","Resurrect an evil wizard","Started a D&D session","Punched a wall","Drank oil","Jumped in a pool of lava","Destroyed an airplane","Went to another region","Presented mattino 5","Decided to start a journey","Jumped from a bridge","Went to a strip club","Got naked","Never woke up again","Got punched in the face","Ate 6 raw tomatoes and got ill","Destroyed the government","Ripped his shirt","The doll became a real man","Ate bread","Got a fever","Made friends","Punched a wall","Head butted Maria de Filippi","Karate chopped car","Learned to breakdance","The sun exploded","A star collapsed"]
    
    var untilFinally: [String] = ["Died","Married","Bought an house","Became rich","Became fit","Became muscular","Defeat fears","Made friends","Ate the forbidden food","Became a comedian","Created a new country","The ritual was complete","Changed his clothes","Killed him","Raided the bank","Made a dream come true","Went to the moon","Adopted a dog","Bought a tv","Wrote a book","Became skinny","Lost weight","Unlocked the hidden powers","Sat down on a chair","Discovered himself","Found the hidden treasure","Summoned the desired deity","Found the lost pirate ship","Had a child","Lost all his belongings","The virus was freed","The window was broken","The bycicle was repaired","Defeated the evil guys","Conquered the desired love","Learned to walk again","Crushed someone in a tv","Escaped from an alternate reality","The aliens defeated the humans"]
    
    var everSinceThen: [String] = ["Lived happily after","Ate all food in the fridge everyday","Never feared pears anymore","Lived in a fridge","Removed everyone’s clothes everyday","Started to hate each other","Killed all farmers in the world","Made Silvio Berlusconi cry everyday","Defeated all of the bad people","Built an empire","Destroyed the universe","Became the hero who saved everyone","Bought anything he wanted","Every child in the world became happy","All the clowns in the world went to the moon","All the Italian television went down","The recipe of the secret sauce was discovered","Never ate sugar again","Never doubted anybody ever again","The zombies kept killing people","Never played games anymore","Got sent to prison","Never resurrected the dead ever again","Every window in the building exploded","Aliens conquered earth"]
    
    var one = ""
    var two = ""
    var three = ""
    var four = ""
    var five = ""
    var six = ""
    var seven = ""
    var eight = ""
    
    @IBOutlet weak var oneLabel: UILabel!
    @IBOutlet weak var twoLabel: UILabel!
    @IBOutlet weak var threeLabel: UILabel!
    @IBOutlet weak var fourLabel: UILabel!
    @IBOutlet weak var fiveLabel: UILabel!
    @IBOutlet weak var sixLabel: UILabel!
    @IBOutlet weak var sevenLabel: UILabel!
    @IBOutlet weak var eightLabel: UILabel!
    
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var twoButton: UIButton!
    @IBOutlet weak var threeButton: UIButton!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fiveButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sevenButton: UIButton!
    @IBOutlet weak var eightButton: UIButton!
    
    @IBOutlet weak var starButton: UIButton!
    
    var stringComplete = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        oneLabel.text = one
        oneLabel.sizeToFit()
        oneLabel.center.x = view.center.x
        twoLabel.text = two
        twoLabel.sizeToFit()
        twoLabel.center.x = view.center.x
        threeLabel.text = three
        threeLabel.sizeToFit()
        threeLabel.center.x = view.center.x
        fourLabel.text = four
        fourLabel.sizeToFit()
        fourLabel.center.x = view.center.x
        fiveLabel.text = five
        fiveLabel.sizeToFit()
        fiveLabel.center.x = view.center.x
        sixLabel.text = six
        sixLabel.sizeToFit()
        sixLabel.center.x = view.center.x
        sevenLabel.text = seven
        sevenLabel.sizeToFit()
        sevenLabel.center.x = view.center.x
        eightLabel.text = eight
        eightLabel.sizeToFit()
        eightLabel.center.x = view.center.x
        
        
    }
    @IBAction func oneTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< onceUponATime.count)
        one = onceUponATime[index]
        oneLabel.text = one
        oneLabel.sizeToFit()
        oneLabel.center.x = view.center.x
        hideButton()
    }
    @IBAction func twoTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< everyday.count)
        two = everyday[index]
        twoLabel.text = two
        twoLabel.sizeToFit()
        twoLabel.center.x = view.center.x
        hideButton()
    }
    @IBAction func threeTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< butOneDay.count)
        three = butOneDay[index]
        threeLabel.text = three
        threeLabel.sizeToFit()
        threeLabel.center.x = view.center.x
        hideButton()
    }
    @IBAction func fourTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< becauseOfThat.count)
        four = becauseOfThat[index]
        fourLabel.text = four
        fourLabel.sizeToFit()
        fourLabel.center.x = view.center.x
        hideButton()
    }
    @IBAction func fiveTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< becauseOfThat.count)
        five = becauseOfThat[index]
        fiveLabel.text = five
        fiveLabel.sizeToFit()
        fiveLabel.center.x = view.center.x
        hideButton()
    }
    @IBAction func sixTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< becauseOfThat.count)
        six = becauseOfThat[index]
        sixLabel.text = six
        sixLabel.sizeToFit()
        sixLabel.center.x = view.center.x
        hideButton()
    }
    @IBAction func sevenTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< untilFinally.count)
        seven = untilFinally[index]
        sevenLabel.text = seven
        sevenLabel.sizeToFit()
        sevenLabel.center.x = view.center.x
        hideButton()
    }
    @IBAction func eightTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< everSinceThen.count)
        eight = everSinceThen[index]
        eightLabel.text = eight
        eightLabel.sizeToFit()
        eightLabel.center.x = view.center.x
        hideButton()
    }
    
    
    @IBAction func starPressed(_ sender: Any) {
        stringComplete = "Once upon a time \(one) that everyday \(two) but one day \(three). Because of that \(four), because of that \(five), because of that \(six). Until finally \(seven) and ever since then \(eight)."
        
        starButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        save(name: stringComplete)
        starButton.isEnabled = false
        
        let alert = UIAlertController(title: "Good Job", message: "Your story has been successfully saved in the portfolio!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes, i got it!", style: .default, handler: nil))

        self.present(alert, animated: true)
        
        hideButton()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func save(name: String) {
        let date = NSDate()
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var dateString = dateFormatter.string(from: date as Date)
        
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
        }
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "FullStory", in: managedContext)!
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        person.setValue(name, forKeyPath: "phrase")
        person.setValue(dateString, forKey: "date")
        // 4
        do {
        try managedContext.save()
            fullStory.append(person)
          } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
        }


        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FullStory")
        //3
        do {
        fullStory = try managedContext.fetch(fetchRequest)
          } catch let error as NSError {
        print("Could not fetch. \(error), \(error.userInfo)")
        }

        for index in 0...(fullStory.count-1) {
            print(fullStory[index].value(forKey: "phrase") as? String)

        }

    }
    
    func hideButton() {
        oneButton.isHidden = true
        twoButton.isHidden = true
        threeButton.isHidden = true
        fourButton.isHidden = true
        fiveButton.isHidden = true
        sixButton.isHidden = true
        sevenButton.isHidden = true
        eightButton.isHidden = true
    }
}
