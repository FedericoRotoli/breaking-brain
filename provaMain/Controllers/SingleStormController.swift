//
//  SingleStormController.swift
//  provaMain
//
//  Created by Federico Rotoli on 15/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//


import UIKit

class SingleStormController: UIViewController {

    @IBOutlet var myScrollView: UIScrollView!
    
    @IBOutlet weak var middleButton: UIButton!
    @IBOutlet weak var beginningButton: UIButton!
    @IBOutlet weak var endingButton: UIButton!
    @IBOutlet weak var fullStoryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        beginningButton.layer.cornerRadius = 20
        beginningButton.imageView?.layer.cornerRadius = 20
        middleButton.layer.cornerRadius = 20
        middleButton.imageView?.layer.cornerRadius = 20
        endingButton.layer.cornerRadius = 20
        endingButton.imageView?.layer.cornerRadius = 20
        fullStoryButton.layer.cornerRadius = 20
        fullStoryButton.imageView?.layer.cornerRadius = 20

        
        myScrollView.contentSize = CGSize(width: view.frame.width, height: (view.frame.height*5))
        beginningButton.frame = CGRect(x: view.frame.midX - 150, y: view.frame.midY, width: 300, height: 280)
        middleButton.frame = CGRect(x: view.frame.midX - 150, y: view.frame.midY + view.frame.height, width: 300, height: 280)
        endingButton.frame = CGRect(x: view.frame.midX - 150, y: view.frame.midY + view.frame.height, width: 300, height: 280)
        fullStoryButton.frame = CGRect(x: view.frame.midX - 150, y: view.frame.midY + view.frame.height, width: 300, height: 280)
        
        
          
    
        let infoButton = UIButton(type: .infoLight)
        let barButton = UIBarButtonItem(customView: infoButton)
        infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = barButton
    }

    @objc func infoButtonTapped() {
        performSegue(withIdentifier: "infoView", sender: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "infoView" {
            if let nextViewController = segue.destination as? InfoController {
                nextViewController.testo = "This is the section where you can create parts or the whole story by shaking your phone to generate ideas:\n\nBeginning: here you can create the beginning of your story!\n\nMiddle: here you can create the middle part of your story!\n\nEnding: here you can create the ending of your story!\n\nFull story: here you can create a full story from scratch!\n\nHave fun! And a little reminder:\nyou can click on the star to save your creation in the portfolio!\n\nIn the summary you can re-roll a word you don’t like just once!"
                nextViewController.image = UIImage(named: "Personal Storm.png")
                nextViewController.color = "yellow"
            }
        }
    }
}
