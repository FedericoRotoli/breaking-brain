//
//  ShakeController2.swift
//  provaMain
//
//  Created by Federico Rotoli on 20/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

class ShakeController2: UIViewController {
    
    @IBOutlet weak var spinePart: UILabel!
    @IBOutlet weak var phraseLabel: UILabel!
    @IBOutlet weak var shakeLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    var becauseOfThat: [String] = ["Went to Japan","Started to go to the gym","Fight other people","Joined mafia","Joined apple academy","Left school","Met a girl","Met a boy","Started to train","Started to play games","Started to run","Started to do scuba dive","Did the Naruto run","Became an anime fan","Became a pineapple","Ate a pizza","Ate a person","Ate a plasma tv","Run into a wall","Went to the moon","Explore the galaxy","Got scared","Got hurt","Surpassed the speed limit","Became a ghost","Was hungry","Was sad","Started a fight","Became a movie star","Became a pop star","Opened a restaurant","Made a game","Followed a guy","Sent some mails","Captured a tiger","Resurrected a dead body","A giant monster killed someone","Destroyed a wall","Hide something","Hijacked a pc","Stole an horse","Fight a tiger","Became a superhero","Jumped under a car","Died","Resurrect an evil wizard","Started a D&D session","Punched a wall","Drank oil","Jumped in a pool of lava","Destroyed an airplane","Went to another region","Presented mattino 5","Decided to start a journey","Jumped from a bridge","Went to a strip club","Got naked","Never woke up again","Got punched in the face","Ate 6 raw tomatoes and got ill","Destroyed the government","Ripped his shirt","The doll became a real man","Ate bread","Got a fever","Made friends","Punched a wall","Head butted Maria de Filippi","Karate chopped car","Learned to breakdance","The sun exploded","A star collapsed"]
    
    var phrase1 = ""
    var phrase2 = ""
    var phrase3 = ""
    
    var firstTime: Bool = true
    
    var times: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        spinePart.layer.cornerRadius = 5
        UIView.animate(withDuration: 1.0, delay: 0.1, options: [.repeat,.curveEaseInOut,.autoreverse], animations: {
            self.shakeLabel.alpha=0.2
        }, completion: nil)
        
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if firstTime {
            
            firstTime = false
            
            if motion == .motionShake {
                switch times {
                case 0:
                    let index = Int.random(in: 0 ..< becauseOfThat.count)
                    phrase1 = becauseOfThat[index]
                    phraseLabel.text = phrase1
                    phraseLabel.sizeToFit()
                        
                    phraseLabel.center = view.center
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                case 1:
                    let index = Int.random(in: 0 ..< becauseOfThat.count)
                    phrase2 = becauseOfThat[index]
                    phraseLabel.text = phrase2
                    phraseLabel.sizeToFit()
                        
                    phraseLabel.center = view.center
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                    
                default:
                    let index = Int.random(in: 0 ..< becauseOfThat.count)
                    phrase3 = becauseOfThat[index]
                    phraseLabel.text = phrase3
                    phraseLabel.sizeToFit()
                        
                    phraseLabel.center = view.center
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                    
                }
                
            }
            
        }


    }
    
    @IBAction func nextStep(_ sender: Any) {
        
        firstTime = true
        
        switch times {
        case 0:
            
            times += 1
            nextButton.isEnabled = false
            shakeLabel.isHidden = false
            
            phraseLabel.text = "Get ready to storm!"
            phraseLabel.sizeToFit()
            phraseLabel.center = view.center
        case 1:
            times += 1
            nextButton.isEnabled = false
            shakeLabel.isHidden = false
            
            phraseLabel.text = "Get ready to storm!"
            phraseLabel.sizeToFit()
            phraseLabel.center = view.center
        default:
            performSegue(withIdentifier: "reviewView2", sender: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "reviewView2" {
            if let nextViewController = segue.destination as? ReviewController2 {
                nextViewController.first = phrase1
                nextViewController.second = phrase2
                nextViewController.third = phrase3
            }
        }
    }
}
