//
//  PortfolioController.swift
//  provaMain
//
//  Created by Federico Rotoli on 12/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit

class PortfolioController: UIViewController {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var archiveButton: UIButton!
    @IBOutlet weak var generatorButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        archiveButton.layer.cornerRadius = 20
        archiveButton.imageView?.layer.cornerRadius = 20
        
        generatorButton.layer.cornerRadius = 20
        generatorButton.imageView?.layer.cornerRadius = 20
        
    }

    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name.Action.CallVC2Method, object: nil)
        })
        
    }
    
}
