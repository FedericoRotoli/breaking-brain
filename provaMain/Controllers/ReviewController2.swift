//
//  ReviewController2.swift
//  provaMain
//
//  Created by Federico Rotoli on 20/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

class ReviewController2: UITableViewController {
    
    var middle:[NSManagedObject] = []
    
    var becauseOfThat: [String] = ["Went to Japan","Started to go to the gym","Fight other people","Joined mafia","Joined apple academy","Left school","Met a girl","Met a boy","Started to train","Started to play games","Started to run","Started to do scuba dive","Did the Naruto run","Became an anime fan","Became a pineapple","Ate a pizza","Ate a person","Ate a plasma tv","Run into a wall","Went to the moon","Explore the galaxy","Got scared","Got hurt","Surpassed the speed limit","Became a ghost","Was hungry","Was sad","Started a fight","Became a movie star","Became a pop star","Opened a restaurant","Made a game","Followed a guy","Sent some mails","Captured a tiger","Resurrected a dead body","A giant monster killed someone","Destroyed a wall","Hide something","Hijacked a pc","Stole an horse","Fight a tiger","Became a superhero","Jumped under a car","Died","Resurrect an evil wizard","Started a D&D session","Punched a wall","Drank oil","Jumped in a pool of lava","Destroyed an airplane","Went to another region","Presented mattino 5","Decided to start a journey","Jumped from a bridge","Went to a strip club","Got naked","Never woke up again","Got punched in the face","Ate 6 raw tomatoes and got ill","Destroyed the government","Ripped his shirt","The doll became a real man","Ate bread","Got a fever","Made friends","Punched a wall","Head butted Maria de Filippi","Karate chopped car","Learned to breakdance","The sun exploded","A star collapsed"]

    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var firstPhrase: UILabel!
    @IBOutlet weak var secondPhrase: UILabel!
    @IBOutlet weak var thirdPhrase: UILabel!
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    
    var first = ""
    var second = ""
    var third = ""
    
    var stringComplete = ""
    
    var phrase = ""
    
    var firstTime: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstPhrase.text = first
        firstPhrase.sizeToFit()
        firstPhrase.center.x = view.center.x
        
        secondPhrase.text = second
        secondPhrase.sizeToFit()
        secondPhrase.center.x = view.center.x
        
        thirdPhrase.text = third
        thirdPhrase.sizeToFit()
        thirdPhrase.center.x = view.center.x
        
        stringComplete = "Because of that \(first) because of that \(second) because of that \(third)"
    }
    
    @IBAction func doneButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func starButtonTapped(_ sender: Any) {
        starButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        save(name: stringComplete)
        starButton.isEnabled = false
        
        let alert = UIAlertController(title: "Good Job", message: "Your story has been successfully saved in the portfolio!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes, i got it!", style: .default, handler: nil))

        self.present(alert, animated: true)
        
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true

    }
    
    @IBAction func firstTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< becauseOfThat.count)
        phrase = becauseOfThat[index]
        firstPhrase.text = phrase
        firstPhrase.sizeToFit()
        firstPhrase.center.x = view.center.x
        
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
    }
    @IBAction func secondTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< becauseOfThat.count)
        phrase = becauseOfThat[index]
        secondPhrase.text = phrase
        secondPhrase.sizeToFit()
        secondPhrase.center.x = view.center.x
        
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
    }
    @IBAction func thirdTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< becauseOfThat.count)
        phrase = becauseOfThat[index]
        thirdPhrase.text = phrase
        thirdPhrase.sizeToFit()
        thirdPhrase.center.x = view.center.x
        
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
    }
    
    func save(name: String) {
        let date = NSDate()
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var dateString = dateFormatter.string(from: date as Date)
        
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
        }
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Middle", in: managedContext)!
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        person.setValue(name, forKeyPath: "phrase")
        person.setValue(dateString, forKeyPath: "date")
        // 4
        do {
        try managedContext.save()
            middle.append(person)
          } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
        }


    

    }
}
