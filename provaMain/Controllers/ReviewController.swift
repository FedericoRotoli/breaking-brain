//
//  ReviewController.swift
//  provaMain
//
//  Created by Federico Rotoli on 20/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

class ReviewController: UITableViewController {
    
    var beginning: [NSManagedObject] = []
    
    var onceUponATime: [String] = ["An accountant","A giant octopus","A pizzaman","A pizza with long legs","A giant walking gun","A tank driver","A superhero","A pumpkin man","A cyclist","A coder","A designer","Beppe Braida","A personal trainer","A boat that can talk","A runner","A comedian","The tallest man in the world","A goblin","A creature made of ink","A jellyfish made of chocolate","A baguette named John","A flower named flower","A clown","A jedi","A table named jerry","A soldier with a water gun","Bruno Vespa","Keanu Reeves","A giant hound dog with 4 hands","A bald guy with 6 hands","A sentient sword","An apple shaped dragon","A dog","A policeman","A knight","A man","An alien","A cat","A dragon","A boy named Franco","Maurizio Costanzo","A purple sponge","A boat with eyes","Thomas the train","A goat","A Nerdy guy","An astronaut","A 5 legged apple","A samurai ninja","A vegetarian shark","A vegan lion","A farmer","An apple engineer","A banana with muscles","A man without pants","Son Goku","A giant","A dog that can talk","A warrior","A Neapolitan","A pizza delivery guy","Mentor Francesco","A giraffe with IQ of 600","Claudio Bisio"]
    
    var everyday: [String] = ["Woke up","Counted the stars","Fight","Went to jogging","Made cakes","Cooked","Did codes","Did design","Liked to read","Liked to eat","Killed someone","Stole from someone","Used a sword","Made cookies","Jumped the rope","Watched anime","Watched movies","Feared fruits","Threw rocks trees","Trained","Liked to drive","Works at an animal shop","Hated to be himself","People passed by without saying hi","Ate unhealthy food","Was struggling with excess fat","Invented something","Beat up evil guys","Built bridges","Forged swords","Jumped a rope","Learned something new","Studied Japanese","Combed other people hairs"]
    
    var butOneDay: [String] = ["Went away","Fled","Got lost","Fell","Ate an entire bowl of pasta","The pc was broken","Found a treasure","Wanted to become stronger","Broke a window","Wanted to become taller","Wanted to become better","Surpassed the speed limit","Died","A friend arrived","Started to drive a car","Stole something","Ate a magician","Adopted a yeti","Started to eat healthy","Killed an alien","Bought an house","Became a magician","Stole an iPhone","Stole a pair of shoes","Ate a table","Forged excalibur","Stole a television","Bought a dinosaur","Traveled around the world","Got a bad bad fever","Opened the forbidden door of a temple","Discovered the fountain of youth","Learned the Kamehameha","Discovered hidden powers","Kicked a guy to the moon","Survived an earthquake"]

    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var firstPhrase: UILabel!
    @IBOutlet weak var secondPhrase: UILabel!
    @IBOutlet weak var thirdPhrase: UILabel!
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    
    var first = ""
    var second = ""
    var third = ""
    
    var phrase = ""
    
    var firstTime: Bool = true
    
    var stringComplete = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstPhrase.text = first
        firstPhrase.sizeToFit()
        firstPhrase.center.x = view.center.x
        
        secondPhrase.text = second
        secondPhrase.sizeToFit()
        secondPhrase.center.x = view.center.x
        
        thirdPhrase.text = third
        thirdPhrase.sizeToFit()
        thirdPhrase.center.x = view.center.x
        
        stringComplete = "Once upon a time \(first) that everyday \(second) but one day \(third)"
    }
    
    @IBAction func doneButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)

        
    }
    
    @IBAction func starButtonTapped(_ sender: Any) {
        starButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        save(name: stringComplete)
        starButton.isEnabled = false
        
        let alert = UIAlertController(title: "Good Job", message: "Your story has been successfully saved in the portfolio!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes, i got it!", style: .default, handler: nil))

        self.present(alert, animated: true)
        
        helpLabel.isHidden = true
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
    }
    
    @IBAction func firstTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< onceUponATime.count)
        phrase = onceUponATime[index]
        firstPhrase.text = phrase
        firstPhrase.sizeToFit()
        firstPhrase.center.x = view.center.x
        
        helpLabel.isHidden = true
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
    }
    @IBAction func secondTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< everyday.count)
        phrase = everyday[index]
        secondPhrase.text = phrase
        secondPhrase.sizeToFit()
        secondPhrase.center.x = view.center.x
        
        helpLabel.isHidden = true
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
    }
    @IBAction func thirdTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< butOneDay.count)
        phrase = butOneDay[index]
        thirdPhrase.text = phrase
        thirdPhrase.sizeToFit()
        thirdPhrase.center.x = view.center.x
        
        helpLabel.isHidden = true
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
    }
    
    func save(name: String) {
        let date = NSDate()
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var dateString = dateFormatter.string(from: date as Date)
    
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
        }
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Beginning", in: managedContext)!
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        person.setValue(name, forKeyPath: "phrase")
        person.setValue(dateString, forKeyPath: "date")
        // 4
        do {
        try managedContext.save()
            beginning.append(person)
          } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
        }
        

    }
    
}
