//
//  StoryModifier.swift
//  provaMain
//
//  Created by Gennaro Frezzetti on 21/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

class StoryModifier : UIViewController {
    
    var db: [NSManagedObject] = []
    
    var data = ""
    
    var startStory = ""
    
    @IBOutlet var story: UITextView!
    @IBOutlet var back: UIBarButtonItem!
    @IBOutlet var save: UIBarButtonItem!
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        if (sezione == 0){
            save(name: "Beginning")
        } else if (sezione == 1) {
            save(name: "Middle")
            
        } else if (sezione == 2) {
            save(name: "Ending")
            
        } else if (sezione == 3) {
            save(name: "FullStory")
            
        }
        
        dismiss(animated: true, completion: nil)
            
        NotificationCenter.default.post(name: Notification.Name.Action.reload, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
        
        startStory = db[myIndex].value(forKey: "phrase") as! String
        story.text = startStory
        
    }
    
    func fetch(){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: database)
        //3
        do {
            db = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            
        }
    }
    
    func save(name: String) {
        let date = NSDate()
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var dateString = dateFormatter.string(from: date as Date)
    
        
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: database, in: managedContext)
        let request = NSFetchRequest<NSFetchRequestResult>()
                request.entity = entity
                let predicate = NSPredicate(format: "(phrase = %@)", startStory)
                request.predicate = predicate
                do {
                    var results = try managedContext.fetch(request)
                    let objectUpdate = results[0] as! NSManagedObject
                    objectUpdate.setValue(story.text!, forKey: "phrase")
                    objectUpdate.setValue(dateString, forKey: "date")
                    do {
                        try managedContext.save()
                    }catch let error as NSError {
                        print(error)
                    }
                }
                catch let error as NSError {
                    print(error)
                }

    }
}
