//
//  GeneratorController.swift
//  provaMain
//
//  Created by Federico Rotoli on 18/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

class GeneratorController: UIViewController {
    
    var db: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    
        let infoButton = UIButton(type: .infoLight)
        let barButton = UIBarButtonItem(customView: infoButton)
        infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = barButton
        
        checkDatabase()
    }
    
    @IBOutlet weak var Begin: UITextView!
    
    @IBOutlet weak var Middle: UITextView!
    
    @IBOutlet weak var END: UITextView!
    
    @objc func infoButtonTapped() {
        performSegue(withIdentifier: "infoView4", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "infoView4" {
            if let nextViewController = segue.destination as? InfoController {
                nextViewController.testo = "In this section you can randomly create a story with your saved creations made in “Storm”\n\nTry to create a full story if you can’t come up with one!\n\n Have Fun!"
                nextViewController.color = "purple"
                nextViewController.image = UIImage(named: "Portfolio Generator.png")
            }
        }
    }
    
    
    //shakeAndGo
    override func becomeFirstResponder() -> Bool {
        return true
    }

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?){
        if motion == .motionShake {
            
            fetch(x: "Beginning")
            
            var index = Int.random(in: 0 ..< db.count)
            var story = db[index]
            
            Begin.text = story.value(forKey: "phrase") as? String
            
            fetch(x: "Middle")
            
            index = Int.random(in: 0 ..< db.count)
            story = db[index]
            Middle.text = story.value(forKey: "phrase") as? String
            
            fetch(x: "Ending")
            
            index = Int.random(in: 0 ..< db.count)
            story = db[index]
            END.text = story.value(forKey: "phrase") as? String
        }
        
    }
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func fetch(x: String){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: x)
        //3
        do {
            db = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            
        }
    }
    
    func checkDatabase() {
        var alert = false
        
        fetch(x: "Beginning")
        if db.count <= 0 {
            alert = true
        }
        
        fetch(x: "Middle")
        if db.count <= 0 {
            alert = true
        }
        
        fetch(x: "Ending")
        if db.count <= 0 {
            alert = true
        }
        
        fetch(x: "FullStory")
        if db.count <= 0 {
            alert = true
        }
        
        if alert {
            let pop = UIAlertController(title: "Warning", message: "You don't have enough stories for the Portfolio Generator!", preferredStyle: .alert)

            pop.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert: UIAlertAction!) in
                self.dismiss(animated: true, completion: nil)
            }))

            self.present(pop, animated: true)
        }
    }
}


