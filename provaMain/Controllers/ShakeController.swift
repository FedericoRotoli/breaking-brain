//
//  ShakeController.swift
//  provaMain
//
//  Created by Federico Rotoli on 19/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

class ShakeController: UIViewController {
    
    @IBOutlet weak var spinePart: UILabel!
    @IBOutlet weak var phraseLabel: UILabel!
    @IBOutlet weak var shakeLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    var onceUponATime: [String] = ["An accountant","A giant octopus","A pizzaman","A pizza with long legs","A giant walking gun","A tank driver","A superhero","A pumpkin man","A cyclist","A coder","A designer","Beppe Braida","A personal trainer","A boat that can talk","A runner","A comedian","The tallest man in the world","A goblin","A creature made of ink","A jellyfish made of chocolate","A baguette named John","A flower named flower","A clown","A jedi","A table named jerry","A soldier with a water gun","Bruno Vespa","Keanu Reeves","A giant hound dog with 4 hands","A bald guy with 6 hands","A sentient sword","An apple shaped dragon","A dog","A policeman","A knight","A man","An alien","A cat","A dragon","A boy named Franco","Maurizio Costanzo","A purple sponge","A boat with eyes","Thomas the train","A goat","A Nerdy guy","An astronaut","A 5 legged apple","A samurai ninja","A vegetarian shark","A vegan lion","A farmer","An apple engineer","A banana with muscles","A man without pants","Son Goku","A giant","A dog that can talk","A warrior","A Neapolitan","A pizza delivery guy","Mentor Francesco","A giraffe with IQ of 600","Claudio Bisio"]
    
    var everyday: [String] = ["Woke up","Counted the stars","Fight","Went to jogging","Made cakes","Cooked","Did codes","Did design","Liked to read","Liked to eat","Killed someone","Stole from someone","Used a sword","Made cookies","Jumped the rope","Watched anime","Watched movies","Feared fruits","Threw rocks trees","Trained","Liked to drive","Works at an animal shop","Hated to be himself","People passed by without saying hi","Ate unhealthy food","Was struggling with excess fat","Invented something","Beat up evil guys","Built bridges","Forged swords","Jumped a rope","Learned something new","Studied Japanese","Combed other people hairs"]
    
    var butOneDay: [String] = ["Went away","Fled","Got lost","Fell","Ate an entire bowl of pasta","The pc was broken","Found a treasure","Wanted to become stronger","Broke a window","Wanted to become taller","Wanted to become better","Surpassed the speed limit","Died","A friend arrived","Started to drive a car","Stole something","Ate a magician","Adopted a yeti","Started to eat healthy","Killed an alien","Bought an house","Became a magician","Stole an iPhone","Stole a pair of shoes","Ate a table","Forged excalibur","Stole a television","Bought a dinosaur","Traveled around the world","Got a bad bad fever","Opened the forbidden door of a temple","Discovered the fountain of youth","Learned the Kamehameha","Discovered hidden powers","Kicked a guy to the moon","Survived an earthquake"]
    
    var phrase1 = ""
    var phrase2 = ""
    var phrase3 = ""
    
    var firstTime: Bool = true
    
    var times: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        spinePart.layer.cornerRadius = 5
        UIView.animate(withDuration: 1.0, delay: 0.1, options: [.repeat,.curveEaseInOut,.autoreverse], animations: {
            self.shakeLabel.alpha=0.2
        }, completion: nil)
        
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if firstTime {
            
            firstTime = false
            
            if motion == .motionShake {
                switch times {
                case 0:
                    let index = Int.random(in: 0 ..< onceUponATime.count)
                    phrase1 = onceUponATime[index]
                    phraseLabel.text = phrase1
                    phraseLabel.sizeToFit()
                        
                    phraseLabel.center.x = view.center.x
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                case 1:
                    let index = Int.random(in: 0 ..< everyday.count)
                    phrase2 = everyday[index]
                    phraseLabel.text = phrase2
                    phraseLabel.sizeToFit()
                        
                    phraseLabel.center.x = view.center.x
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                    
                default:
                    let index = Int.random(in: 0 ..< butOneDay.count)
                    phrase3 = butOneDay[index]
                    phraseLabel.text = phrase3
                    phraseLabel.sizeToFit()
                        
                    phraseLabel.center.x = view.center.x
                nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                    
                }
            }
            
        }


    }
    
    @IBAction func nextStep(_ sender: Any) {
        
        firstTime = true
        
        switch times {
        case 0:
            
            times += 1
            nextButton.isEnabled = false
            shakeLabel.isHidden = false
            
            spinePart.text = "Everyday"
            
            spinePart.sizeToFit()
            spinePart.center.x = view.center.x
            
            phraseLabel.text = "Get ready to storm!"
            phraseLabel.sizeToFit()
            phraseLabel.center = view.center
        case 1:
            times += 1
            nextButton.isEnabled = false
            shakeLabel.isHidden = false
            
            spinePart.text = "But One Day"
            
            spinePart.sizeToFit()
            spinePart.center.x = view.center.x
            
            phraseLabel.text = "Get ready to storm!"
            phraseLabel.sizeToFit()
            phraseLabel.center = view.center
        default:
            performSegue(withIdentifier: "reviewView", sender: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "reviewView" {
            if let nextViewController = segue.destination as? ReviewController {
                nextViewController.first = phrase1
                nextViewController.second = phrase2
                nextViewController.third = phrase3
            }
        }
    }
}
