//
//  ModifyStoryController.swift
//  provaMain
//
//  Created by Gennaro Frezzetti on 21/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit

class ModifyStoryController : UIViewController {
    
    @IBOutlet var backButton: UIBarButtonItem!
    @IBOutlet var storyText: UITextView!
    
    @IBAction func backFunc(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
