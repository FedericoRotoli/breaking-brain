//
//  SettingsController.swift
//  provaMain
//
//  Created by Federico Rotoli on 15/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//


import UIKit
import AVFoundation


class SettingsController: UITableViewController {
    
    @IBOutlet var soundSwitch: UISwitch!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var creditsButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nome = UserDefaults.standard.string(forKey: "name") {
            nameTextField.text = nome
        } else {
            nameTextField.text = "Guest"
        }
        
    }
    
    @IBAction func closeButton(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    @IBAction func muteSwitch(_ sender: Any) {
//        if (soundSwitch.isOn) {
//            NotificationCenter.default.post(name: Notification.Name.Action.playmusic, object: nil)
//        } else {
//            NotificationCenter.default.post(name: Notification.Name.Action.stopmusic, object: nil)
//
//        }
    }
    @IBAction func nameChanged(_ sender: Any) {
        UserDefaults.standard.set(nameTextField.text, forKey: "name")
        
        NotificationCenter.default.post(name: Notification.Name.Action.nameChange, object: nil)
    }
    
    @IBAction func credits(_ sender: Any) {
        let alert = UIAlertController(title: "Credits", message: "App created by:\n AppleMetalJacket", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))

        self.present(alert, animated: true)
    }
    
    
}
