//
//  FullStoryController.swift
//  provaMain
//
//  Created by Federico Rotoli on 22/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit

class FullStoryController: UIViewController {
    
    var onceUponATime: [String] = ["An accountant","A giant octopus","A pizzaman","A pizza with long legs","A giant walking gun","A tank driver","A superhero","A pumpkin man","A cyclist","A coder","A designer","Beppe Braida","A personal trainer","A boat that can talk","A runner","A comedian","The tallest man in the world","A goblin","A creature made of ink","A jellyfish made of chocolate","A baguette named John","A flower named flower","A clown","A jedi","A table named jerry","A soldier with a water gun","Bruno Vespa","Keanu Reeves","A giant hound dog with 4 hands","A bald guy with 6 hands","A sentient sword","An apple shaped dragon","A dog","A policeman","A knight","A man","An alien","A cat","A dragon","A boy named Franco","Maurizio Costanzo","A purple sponge","A boat with eyes","Thomas the train","A goat","A Nerdy guy","An astronaut","A 5 legged apple","A samurai ninja","A vegetarian shark","A vegan lion","A farmer","An apple engineer","A banana with muscles","A man without pants","Son Goku","A giant","A dog that can talk","A warrior","A Neapolitan","A pizza delivery guy","Mentor Francesco","A giraffe with IQ of 600","Claudio Bisio"]
    
    var everyday: [String] = ["Woke up","Counted the stars","Fight","Went to jogging","Made cakes","Cooked","Did codes","Did design","Liked to read","Liked to eat","Killed someone","Stole from someone","Used a sword","Made cookies","Jumped the rope","Watched anime","Watched movies","Feared fruits","Threw rocks trees","Trained","Liked to drive","Works at an animal shop","Hated to be himself","People passed by without saying hi","Ate unhealthy food","Was struggling with excess fat","Invented something","Beat up evil guys","Built bridges","Forged swords","Jumped a rope","Learned something new","Studied Japanese","Combed other people hairs"]
    
    var butOneDay: [String] = ["Went away","Fled","Got lost","Fell","Ate an entire bowl of pasta","The pc was broken","Found a treasure","Wanted to become stronger","Broke a window","Wanted to become taller","Wanted to become better","Surpassed the speed limit","Died","A friend arrived","Started to drive a car","Stole something","Ate a magician","Adopted a yeti","Started to eat healthy","Killed an alien","Bought an house","Became a magician","Stole an iPhone","Stole a pair of shoes","Ate a table","Forged excalibur","Stole a television","Bought a dinosaur","Traveled around the world","Got a bad bad fever","Opened the forbidden door of a temple","Discovered the fountain of youth","Learned the Kamehameha","Discovered hidden powers","Kicked a guy to the moon","Survived an earthquake"]
    
    var becauseOfThat: [String] = ["Went to Japan","Started to go to the gym","Fight other people","Joined mafia","Joined apple academy","Left school","Met a girl","Met a boy","Started to train","Started to play games","Started to run","Started to do scuba dive","Did the Naruto run","Became an anime fan","Became a pineapple","Ate a pizza","Ate a person","Ate a plasma tv","Run into a wall","Went to the moon","Explore the galaxy","Got scared","Got hurt","Surpassed the speed limit","Became a ghost","Was hungry","Was sad","Started a fight","Became a movie star","Became a pop star","Opened a restaurant","Made a game","Followed a guy","Sent some mails","Captured a tiger","Resurrected a dead body","A giant monster killed someone","Destroyed a wall","Hide something","Hijacked a pc","Stole an horse","Fight a tiger","Became a superhero","Jumped under a car","Died","Resurrect an evil wizard","Started a D&D session","Punched a wall","Drank oil","Jumped in a pool of lava","Destroyed an airplane","Went to another region","Presented mattino 5","Decided to start a journey","Jumped from a bridge","Went to a strip club","Got naked","Never woke up again","Got punched in the face","Ate 6 raw tomatoes and got ill","Destroyed the government","Ripped his shirt","The doll became a real man","Ate bread","Got a fever","Made friends","Punched a wall","Head butted Maria de Filippi","Karate chopped car","Learned to breakdance","The sun exploded","A star collapsed"]
    
    var untilFinally: [String] = ["Died","Married","Bought an house","Became rich","Became fit","Became muscular","Defeat fears","Made friends","Ate the forbidden food","Became a comedian","Created a new country","The ritual was complete","Changed his clothes","Killed him","Raided the bank","Made a dream come true","Went to the moon","Adopted a dog","Bought a tv","Wrote a book","Became skinny","Lost weight","Unlocked the hidden powers","Sat down on a chair","Discovered himself","Found the hidden treasure","Summoned the desired deity","Found the lost pirate ship","Had a child","Lost all his belongings","The virus was freed","The window was broken","The bycicle was repaired","Defeated the evil guys","Conquered the desired love","Learned to walk again","Crushed someone in a tv","Escaped from an alternate reality","The aliens defeated the humans"]
    
    var everSinceThen: [String] = ["Lived happily after","Ate all food in the fridge everyday","Never feared pears anymore","Lived in a fridge","Removed everyone’s clothes everyday","Started to hate each other","Killed all farmers in the world","Made Silvio Berlusconi cry everyday","Defeated all of the bad people","Built an empire","Destroyed the universe","Became the hero who saved everyone","Bought anything he wanted","Every child in the world became happy","All the clowns in the world went to the moon","All the Italian television went down","The recipe of the secret sauce was discovered","Never ate sugar again","Never doubted anybody ever again","The zombies kept killing people","Never played games anymore","Got sent to prison","Never resurrected the dead ever again","Every window in the building exploded","Aliens conquered earth"]
    
    @IBOutlet weak var shakeLabel: UILabel!
    @IBOutlet weak var phaseLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var firstTitle: UILabel!
    @IBOutlet weak var secondTitle: UILabel!
    @IBOutlet weak var thirdTitle: UILabel!
    
    @IBOutlet weak var firstText: UILabel!
    @IBOutlet weak var secondText: UILabel!
    @IBOutlet weak var thirdText: UILabel!
    
    var phrase1 = ""
    var phrase2 = ""
    var phrase3 = ""
    var phrase4 = ""
    var phrase5 = ""
    var phrase6 = ""
    var phrase7 = ""
    var phrase8 = ""
    
    var firstTime = true
    var cont = 0
    var times = 0
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        phaseLabel.text = "Beginning"
        phaseLabel.sizeToFit()
        phaseLabel.center.x = view.center.x
        
        UIView.animate(withDuration: 1.0, delay: 0.1, options: [.repeat,.curveEaseInOut,.autoreverse], animations: {
            self.shakeLabel.alpha=0.2
        }, completion: nil)
        
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if firstTime {
            
            firstTime = false
            
            if motion == .motionShake {
                
                firstTime=false
                
                switch times {
                case 0:
                    let index0 = Int.random(in: 0 ..< onceUponATime.count)
                    let index1 = Int.random(in: 0 ..< everyday.count)
                    let index2 = Int.random(in: 0 ..< butOneDay.count)
                    
                    phrase1 = onceUponATime[index0]
                    phrase2 = everyday[index1]
                    phrase3 = butOneDay[index2]
                    
                    firstText.text = phrase1
                    firstText.sizeToFit()
                    firstText.center.x = view.center.x
                    
                    secondText.text = phrase2
                    secondText.sizeToFit()
                    secondText.center.x = view.center.x
                    
                    thirdText.text = phrase3
                    thirdText.sizeToFit()
                    thirdText.center.x = view.center.x
                    
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                    
                
                case 1:
                    let index0 = Int.random(in: 0 ..< becauseOfThat.count)
                    let index1 = Int.random(in: 0 ..< becauseOfThat.count)
                    let index2 = Int.random(in: 0 ..< becauseOfThat.count)
                    
                    phrase4 = becauseOfThat[index0]
                    phrase5 = becauseOfThat[index1]
                    phrase6 = becauseOfThat[index2]
                    
                    firstText.text = phrase4
                    firstText.sizeToFit()
                    firstText.center.x = view.center.x
                    
                    secondText.text = phrase5
                    secondText.sizeToFit()
                    secondText.center.x = view.center.x
                    
                    thirdText.text = phrase6
                    thirdText.sizeToFit()
                    thirdText.center.x = view.center.x
                    
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                    
                default:
                    
                    let index0 = Int.random(in: 0 ..< untilFinally.count)
                    let index1 = Int.random(in: 0 ..< everSinceThen.count)
                    
                    phrase7 = untilFinally[index0]
                    phrase8 = everSinceThen[index1]
                    
                    firstText.text = phrase7
                    firstText.sizeToFit()
                    firstText.center.x = view.center.x
                    
                    secondText.text = phrase8
                    secondText.sizeToFit()
                    secondText.center.x = view.center.x
                    
                    nextButton.isEnabled = true
                    shakeLabel.isHidden=true
                    
                }
            }
            
        }


    }
    
    @IBAction func nextStep(_ sender: Any) {
        
        firstTime = true
        
        switch times {
        case 0:
            phaseLabel.text = "Middle"
            firstTitle.text = "Because of that"
            firstTitle.sizeToFit()
            firstTitle.center.x = view.center.x
            secondTitle.text = "Because of that"
            secondTitle.sizeToFit()
            secondTitle.center.x = view.center.x
            thirdTitle.text = "Because of that"
            thirdTitle.sizeToFit()
            thirdTitle.center.x = view.center.x
            phaseLabel.sizeToFit()
            phaseLabel.center.x = view.center.x
            nextButton.isEnabled = false
            
            shakeLabel.isHidden = false
            
            firstText.text = "Get ready to storm!"
            firstText.sizeToFit()
            firstText.center.x = view.center.x
            secondText.text = "Get ready to storm!"
            secondText.sizeToFit()
            secondText.center.x = view.center.x
            thirdText.text = "Get ready to storm!"
            thirdText.sizeToFit()
            thirdText.center.x = view.center.x
            
            times += 1
        case 1:
            phaseLabel.text = "Ending"
            firstTitle.text = "Until finally"
            firstTitle.sizeToFit()
            firstTitle.center.x = view.center.x
            secondTitle.text = "And ever since then"
            secondTitle.sizeToFit()
            secondTitle.center.x = view.center.x
            phaseLabel.sizeToFit()
            phaseLabel.center.x = view.center.x
            nextButton.isEnabled = false
            
            shakeLabel.isHidden = false
            
            thirdText.isHidden = true
            thirdTitle.isHidden = true
            
            firstText.text = "Get ready to storm!"
            firstText.sizeToFit()
            firstText.center.x = view.center.x
            secondText.text = "Get ready to storm!"
            secondText.sizeToFit()
            secondText.center.x = view.center.x
            
            times += 1
        default:
            performSegue(withIdentifier: "summaryView", sender: nil)
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "summaryView" {
            if let nextViewController = segue.destination as? FullSummaryController {
                nextViewController.one = phrase1
                nextViewController.two = phrase2
                nextViewController.three = phrase3
                nextViewController.four = phrase4
                nextViewController.five = phrase5
                nextViewController.six = phrase6
                nextViewController.seven = phrase7
                nextViewController.eight = phrase8
                
            }
        }
    }
}
