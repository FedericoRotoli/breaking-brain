//
//  ReviewController3.swift
//  provaMain
//
//  Created by Federico Rotoli on 20/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import CoreData

class ReviewController3: UITableViewController {
    
    var ending:[NSManagedObject] = []
    
    var untilFinally: [String] = ["Died","Married","Bought an house","Became rich","Became fit","Became muscular","Defeat fears","Made friends","Ate the forbidden food","Became a comedian","Created a new country","The ritual was complete","Changed his clothes","Killed him","Raided the bank","Made a dream come true","Went to the moon","Adopted a dog","Bought a tv","Wrote a book","Became skinny","Lost weight","Unlocked the hidden powers","Sat down on a chair","Discovered himself","Found the hidden treasure","Summoned the desired deity","Found the lost pirate ship","Had a child","Lost all his belongings","The virus was freed","The window was broken","The bycicle was repaired","Defeated the evil guys","Conquered the desired love","Learned to walk again","Crushed someone in a tv","Escaped from an alternate reality","The aliens defeated the humans"]
    
    var everSinceThen: [String] = ["Lived happily after","Ate all food in the fridge everyday","Never feared pears anymore","Lived in a fridge","Removed everyone’s clothes everyday","Started to hate each other","Killed all farmers in the world","Made Silvio Berlusconi cry everyday","Defeated all of the bad people","Built an empire","Destroyed the universe","Became the hero who saved everyone","Bought anything he wanted","Every child in the world became happy","All the clowns in the world went to the moon","All the Italian television went down","The recipe of the secret sauce was discovered","Never ate sugar again","Never doubted anybody ever again","The zombies kept killing people","Never played games anymore","Got sent to prison","Never resurrected the dead ever again","Every window in the building exploded","Aliens conquered earth"]

    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var firstPhrase: UILabel!
    @IBOutlet weak var thirdPhrase: UILabel!
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    
    var first = ""
    var third = ""
    
    var phrase = ""
    
    var firstTime: Bool = true
    
    var stringComplete = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstPhrase.text = first
        firstPhrase.sizeToFit()
        firstPhrase.center.x = view.center.x
        
        thirdPhrase.text = third
        thirdPhrase.sizeToFit()
        thirdPhrase.center.x = view.center.x
        
        stringComplete = "Until finally \(first) and ever since then \(third)"
    }
    
    @IBAction func doneButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func starButtonTapped(_ sender: Any) {
        starButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        save(name: stringComplete)
        starButton.isEnabled = false
        
        let alert = UIAlertController(title: "Good Job", message: "Your story has been successfully saved in the portfolio!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes, i got it!", style: .default, handler: nil))

        self.present(alert, animated: true)
        
        firstButton.isHidden = true
        thirdButton.isHidden = true
    }
    
    @IBAction func firstTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< untilFinally.count)
        phrase = untilFinally[index]
        firstPhrase.text = phrase
        firstPhrase.sizeToFit()
        firstPhrase.center.x = view.center.x
        
        firstButton.isHidden = true
        thirdButton.isHidden = true
    }

    @IBAction func thirdTapped(_ sender: Any) {
        let index = Int.random(in: 0 ..< everSinceThen.count)
        phrase = everSinceThen[index]
        thirdPhrase.text = phrase
        thirdPhrase.sizeToFit()
        thirdPhrase.center.x = view.center.x
        
        firstButton.isHidden = true
        thirdButton.isHidden = true
    }
    
    func save(name: String) {
        let date = NSDate()
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var dateString = dateFormatter.string(from: date as Date)
        
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
        }
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Ending", in: managedContext)!
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        person.setValue(name, forKeyPath: "phrase")
        person.setValue(dateString, forKey: "date")
        // 4
        do {
        try managedContext.save()
            ending.append(person)
          } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
        }


    }
}
