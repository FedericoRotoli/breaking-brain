//
//  StormController.swift
//  provaMain
//
//  Created by Federico Rotoli on 12/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit

class StormController: UIViewController {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var singleStormButton: UIButton!
    @IBOutlet weak var teamStormButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        singleStormButton.layer.cornerRadius = 20
        singleStormButton.imageView?.layer.cornerRadius = 20
        
        teamStormButton.layer.cornerRadius = 20
        teamStormButton.imageView?.layer.cornerRadius = 20
        
        // Do any additional setup after loading the view.
    }


    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name.Action.CallVC1Method, object: nil)
        })
    }
}
