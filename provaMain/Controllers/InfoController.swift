//
//  InfoController.swift
//  provaMain
//
//  Created by Federico Rotoli on 15/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit

class InfoController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var myImageView: UIImageView!
    
    var testo = ""
    var image: UIImage!
    var color = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstLabel.text = testo
        firstLabel.sizeToFit()
        firstLabel.center = view.center
        myImageView.image = image
        myImageView.layer.cornerRadius = 15.0
        myImageView.clipsToBounds = true
        
        if color=="yellow" {
            firstLabel.textColor = .white
            view.backgroundColor = UIColor(red: 231/255, green: 168/255, blue: 56/255, alpha: 1)
            titleLabel.textColor = .white
        }
        if color=="purple" {
            firstLabel.textColor = .white
            view.backgroundColor = UIColor(red: 88/255, green: 69/255, blue: 132/255, alpha: 1)
            titleLabel.textColor = .white
        }
    }
    
}
