//
//  ViewController.swift
//  provaMain
//
//  Created by Federico Rotoli on 12/11/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import AVFoundation

extension Notification.Name {
    struct Action {
        //notification name
        static let CallVC1Method = Notification.Name("CallVC1Method")
        static let CallVC2Method = Notification.Name("CallVC2Method")
        static let playmusic =
            Notification.Name("playmusic")
        static let stopmusic =
            Notification.Name("stopmusic")
        static let nameChange = Notification.Name("nameChange")
        static let reload = Notification.Name("reload")

    }
}

class ViewController: UIViewController, UIScrollViewDelegate {
    
    
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var stormButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var portfolioButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    
    var contentWidht: CGFloat = 0.0
    
    var oldValue: CGRect!
    var oldValue2: CGRect!

    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let sound = Bundle.main.path(forResource: "music", ofType: "mp3")
//        
//        do {
//            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
//        } catch {
//            print("error")
//        }
//        audioPlayer?.play()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(back), name: Notification.Name.Action.CallVC1Method, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(back2), name: Notification.Name.Action.CallVC2Method, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playthemusic), name: Notification.Name.Action.playmusic, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(stopthemusic), name: Notification.Name.Action.stopmusic, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(nameChange), name: Notification.Name.Action.nameChange, object: nil)
        
        myScrollView.delegate = self
        
        stormButton.frame = CGRect(x: (view.frame.midX)-125, y: view.frame.midY-250, width: view.frame.width-150, height: 400)
        stormButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        stormButton.layer.shadowOffset = CGSize(width: 3.0, height: 5.0)
        stormButton.layer.shadowOpacity = 1.0
        stormButton.imageView?.layer.cornerRadius = 20
        stormButton.layer.cornerRadius=20

        portfolioButton.frame = CGRect(x: (view.frame.midX + view.frame.width)-225, y: view.frame.midY-250, width: view.frame.width-150, height: 400)
        portfolioButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        portfolioButton.layer.shadowOffset = CGSize(width: 3.0, height: 5.0)
        portfolioButton.layer.shadowOpacity = 1.0
        portfolioButton.imageView?.layer.cornerRadius = 20
        portfolioButton.layer.cornerRadius=20
        
        oldValue2 = stormButton.frame
        oldValue = portfolioButton.frame

        contentWidht = view.frame.width*2-100
        myScrollView.contentSize = CGSize(width: contentWidht, height: view.frame.height)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let nome = UserDefaults.standard.string(forKey: "name") {
            nameLabel.text = "Hello " + nome
        } else {
            nameLabel.text = "Hello Guest"
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
           scrollView.contentOffset.y = 0
        }
        
        if myScrollView.contentOffset.x == 0 {
            infoLabel.text = "This is the Storm section where you             can train your creativity and find new ideas!This is based on the story spine method              by Ken Adams"
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {

                self.infoLabel.alpha = 1

            })


        } else if myScrollView.contentOffset.x > 200 {
            infoLabel.text = "This is the Portfolio section where you\ncan store your creations and\ndevelop them even more!"
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {
                self.infoLabel.alpha = 1

            })
            
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {

                self.infoLabel.alpha = 0

            })
        }


    }

    @IBAction func showStorm(_ sender: Any) {
        if myScrollView.contentOffset.x==0 {
            settingsButton.layer.zPosition = 1
            myScrollView.layer.zPosition = 2
            
            stormButton.layer.zPosition = 2
            portfolioButton.layer.zPosition = 1
            
            UIView.animate(withDuration: 0.7, delay: 0.1, options: .curveEaseInOut, animations: {
                self.stormButton.frame = self.view.frame
                self.stormButton.layer.cornerRadius = 0
            }, completion: {finished in
                self.performSegue(withIdentifier: "stormView",sender: self)
            })
        }

    }
    
    @IBAction func showPortfolio(_ sender: Any) {
        if myScrollView.contentOffset.x>200 {
            settingsButton.layer.zPosition = 1
            myScrollView.layer.zPosition = 2
            
            stormButton.layer.zPosition = 2
            portfolioButton.layer.zPosition = 3
            
            UIView.animate(withDuration: 0.7, delay: 0.1, options: .curveEaseInOut, animations: {
                self.portfolioButton.frame = CGRect(x: self.myScrollView.contentOffset.x, y: self.view.frame.minY, width: self.view.frame.width, height: self.view.frame.height)
                self.portfolioButton.layer.cornerRadius = 0
            }, completion: {finished in
                self.performSegue(withIdentifier: "portfolioView",sender: self)
            })
        }

    }

    
    
    
    @objc func back() {
        
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseInOut, animations: {
            self.stormButton.frame = self.oldValue2
            self.stormButton.layer.cornerRadius = 20
        }, completion: { finished in
            self.settingsButton.layer.zPosition = 1
            self.myScrollView.layer.zPosition = 2
            
            self.stormButton.layer.zPosition = 2
            self.portfolioButton.layer.zPosition = 1
        })
    }
    
    @objc func back2() {
        
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseInOut, animations: {
            self.portfolioButton.frame = self.oldValue
            self.portfolioButton.layer.cornerRadius = 20
        }, completion: { finished in
            self.settingsButton.layer.zPosition = 1
            self.myScrollView.layer.zPosition = 2
            
            self.stormButton.layer.zPosition = 1
            self.portfolioButton.layer.zPosition = 2
        })
    }
    
    @objc func playthemusic() {
        audioPlayer?.play()
    }
    
    @objc func stopthemusic() {
        audioPlayer?.stop()
        
    }
    
    @objc func nameChange() {
        if let nome = UserDefaults.standard.string(forKey: "name") {
            nameLabel.text = "Hello " + nome
        } else {
            nameLabel.text = "Hello Guest"
        }
        
    }
    
}

